import { inject, TestBed } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';
import { SharedModule } from '@app/modules/shared/shared.module';

describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuard, { provide: APP_BASE_HREF, useValue: '/' }],
      imports: [SharedModule, RouterModule.forRoot([])]
    });
  });

  it('should ...', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
