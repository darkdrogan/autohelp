import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';
// tslint:disable-next-line
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';

import { AppState } from '@app/store/root-store.reducer';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private store: Store<AppState>) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.pipe(
      select('auth'),
      map(({ token }) => {
        if (token === '') {
          this.router.navigate(['/login']);
          return false;
        }
        return true;
      })
    );
  }
}
