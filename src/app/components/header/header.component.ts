import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
// tslint:disable-next-line
import { Observable } from 'rxjs';

import { AppState } from '@app/store/root-store.reducer';
import { Logout } from '@app/store/auth/auth.actions';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLoggedIn: Observable<boolean>;

  constructor(private router: Router, private cdr: ChangeDetectorRef, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.isLoggedIn = this.store.pipe(select('auth'));
  }

  logOut() {
    this.store.dispatch(new Logout());
    this.cdr.markForCheck();
    this.router.navigate(['/login']);
  }

  navigateToResultSearch(event: { type: string; input: string; data: any }) {
    let type = '';
    switch (event.type) {
      case 'car':
      case 'governmentNumber':
        type = 'car';
        break;
      case 'client':
        type = 'client';
        break;
      default:
        type = '404';
        break;
    }
    if (Object.keys(event.data).length > 0) {
      this.router.navigate([type, event.input]);
    } else {
      this.router.navigate([`/add_${type}`], { queryParams: { [event.type]: event.input } });
    }
  }
}
