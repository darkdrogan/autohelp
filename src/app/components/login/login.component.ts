import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppState } from '@app/store/root-store.reducer';
import { Store } from '@ngrx/store';
import { TryLogin } from '@app/store/auth/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username = null;
  password: string;

  constructor(private router: Router, private store: Store<AppState>) {}

  ngOnInit() {}

  login() {
    this.store.dispatch(new TryLogin({ email: this.username, password: this.password }));
  }
}
