import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    firebase.initializeApp({
      apiKey: 'AIzaSyC72ATyrkpx3qujdqB3ImZDx5FfngWmeqg',
      authDomain: 'autohelp-fed7a.firebaseapp.com',
      databaseURL: 'https://autohelp-fed7a.firebaseio.com'
    });
  }
}
