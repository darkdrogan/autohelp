import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FirebaseService } from '@app/services/firebase/firebase.service';
// tslint:disable-next-line
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-client-info-edit',
  templateUrl: './client-info-edit.component.html',
  styleUrls: ['./client-info-edit.component.scss']
})
export class ClientInfoEditComponent implements OnInit, OnDestroy {
  clientForm: FormGroup;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private firebaseService: FirebaseService) {}

  ngOnInit() {
    this.clientForm = new FormGroup({
      surname: new FormControl(null),
      name: new FormControl(null, Validators.required),
      middleName: new FormControl(null),
      phoneNumber: new FormControl(null, Validators.required),
      birthdayDate: new FormControl(null),
      note: new FormControl(null)
    });
  }

  onSubmitClient() {
    this.firebaseService
      .addClient(this.clientForm.value)
      .pipe(takeUntil(this.ngUnsubscribe))
      // tslint:disable-next-line
      .subscribe(result => console.log(result));
    this.resetForm();
  }

  resetForm() {
    this.clientForm.reset();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
