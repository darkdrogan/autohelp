import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
// tslint:disable-next-line
import { Subject, Subscription } from 'rxjs';

import { FirebaseService } from '@app/services/firebase/firebase.service';
import { BodyType, TypeOfGasoline, GearBox } from '@app/models/car';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-car-info-edit',
  templateUrl: './car-info-edit.component.html',
  styleUrls: ['./car-info-edit.component.scss']
})
export class CarInfoEditComponent implements OnInit, OnDestroy {
  private ngUnsubscribe$: Subject<void> = new Subject<void>();
  addCarForm: FormGroup;
  bodyType = BodyType;
  typeOfGasoline = TypeOfGasoline;
  gearBox = GearBox;
  bodyTypeKeys;
  typeOfGasolineKeys;
  gearBoxKeys;

  constructor(private firebaseService: FirebaseService) {}

  ngOnInit() {
    this.initializeForm();
    this.bodyTypeKeys = Object.keys(this.bodyType);
    this.typeOfGasolineKeys = Object.keys(this.typeOfGasoline);
    this.gearBoxKeys = Object.keys(this.gearBox);
  }

  initializeForm() {
    this.addCarForm = new FormGroup({
      vin: new FormControl(null, [
        Validators.required,
        Validators.minLength(17),
        Validators.maxLength(17),
        Validators.pattern(/^\w{13}\d{4}$/)
      ]),
      governmentNumber: new FormControl(null, [
        Validators.minLength(7),
        Validators.maxLength(9),
        Validators.pattern(/^[ABCEHKMOPTXY]\d{3}[ABCEHKMOPTXY]{2}\d{2,3}$/)
      ]),
      brand: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      model: new FormControl(null, [Validators.required, Validators.maxLength(15)]),
      year: new FormControl(null, [
        Validators.required,
        Validators.maxLength(4),
        Validators.min(1940),
        Validators.max(2019)
      ]),
      engineDisplacement: new FormControl(null, [Validators.required, Validators.min(0.1), Validators.max(10)]),
      horsePower: new FormControl(null, [Validators.required, Validators.min(10), Validators.max(1500)]),
      engineModel: new FormControl(),
      typeOfGasoline: new FormControl(null, Validators.required),
      bodyType: new FormControl(null, Validators.required),
      gearBox: new FormControl(null, Validators.required)
    });
  }

  onSubmit() {
    this.firebaseService
      .addCar(this.addCarForm.value)
      .pipe(takeUntil(this.ngUnsubscribe$))
      // tslint:disable-next-line
      .subscribe(result => console.log(result), error => console.log(error));
    // tslint:disable-next-line
    this.resetForm();
  }

  resetForm() {
    this.addCarForm.reset();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }
}
