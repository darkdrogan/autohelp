import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from '@app/components/login/login.component';
import { NotFoundComponent } from '@app/components/not-found/not-found.component';
import { AuthGuard } from './guards/auth.guard';

const BASE_PATH = 'app/pages';

const routes: Routes = [
  { path: '', canActivate: [AuthGuard], pathMatch: 'full', redirectTo: 'index' },
  {
    path: 'index',
    canActivate: [AuthGuard],
    loadChildren: `${BASE_PATH}/index/index.module#IndexModule`
  },
  {
    path: 'catalog',
    canActivate: [AuthGuard],
    loadChildren: `${BASE_PATH}/catalog/catalog.module#CatalogModule`
  },
  {
    path: 'add',
    canActivate: [AuthGuard],
    loadChildren: `${BASE_PATH}/car-info-edit/car-info-edit.module#CarInfoEditModule`
  },
  { path: 'login', component: LoginComponent },
  { path: '**', component: NotFoundComponent }
];

export const Routing = RouterModule.forRoot(routes, {
  // enableTracing: true,
  initialNavigation: 'enabled',
  onSameUrlNavigation: 'reload'
});
