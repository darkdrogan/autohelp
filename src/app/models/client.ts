export interface Client {
  id?: number;
  surname?: string;
  name: string;
  middleName?: string;
  phoneNumber: number;
  birthday?: Date;
  note?: string;
  cars?: string[];
  orders?: number[];
}
