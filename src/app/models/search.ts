export interface SearchResult {
  type: string;
  userInput: string;
  isExist?: boolean;
  link?: string;
}
