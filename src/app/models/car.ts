export enum BodyType {
  SEDAN = 'седан',
  HATCH_BACK = 'хетчбек',
  STATION_WAGON = 'универсал',
  SUV = 'внедорожник',
  PICKUP = 'пикап',
  VAN = 'фургон',
  MINIVAN = 'минивэн',
  COUPE = 'купе',
  CABRIOLET = 'кабриолет'
}

export enum TypeOfGasoline {
  GASOLINE = 'бензин',
  DIESEL = 'дизель',
  GAS = 'газ'
}

export enum GearBox {
  auto = 'акпп',
  manual = 'мккп',
  variator = 'вариатор',
  robot = 'робот'
}

export interface Car {
  vin: string;
  governmentNumber: string;
  brand: string;
  model: string;
  year: string;
  engineDisplacement: number;
  horsePower: number;
  typeOfGasoline: TypeOfGasoline;
  bodyType: BodyType;
  engineModel?: string;
  gearBox: GearBox;
  ownerId?: number;
  orders?: number[];
}
