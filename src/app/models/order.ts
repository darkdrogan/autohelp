export interface Repair {
  type: string;
  cost: number;
}

export interface Detail {
  name: string;
  code: string;
  cost: number;
}

export enum OrderStatusTypes {
  CREATED = 'создан',
  CANCELED = 'отменен',
  WAITING_DETAILS = 'ожидание запчастей',
  PAID = 'оплачен',
  CLOSED = 'завершен'
}

export interface OrderStatus {
  status: string;
  statusDate: Date;
}

export interface Order {
  id: string;
  vin: string;
  clientId: number;
  repairList: Repair[];
  details: Detail[];
  orderStatus: OrderStatus[];
}
