import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatInputModule, MatButtonModule, MatDialogModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SearchComponent } from './components/search/search.component';
import { SearchService } from './services/search.service';
// prettier-ignore
import {
  SearchResultDialogComponent
} from './components/search/modals/search-result-dialog/search-result-dialog.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule
  ],
  declarations: [SearchComponent, SearchResultDialogComponent],
  entryComponents: [SearchResultDialogComponent],
  providers: [SearchService],
  exports: [SearchComponent, MatIconModule, MatInputModule, MatButtonModule, MatDialogModule]
})
export class SearchModule {}
