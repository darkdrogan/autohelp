import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Car } from '@app/models/car';
import { Client } from '@app/models/client';
// tslint:disable-next-line
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export const SEARCH_DIALOG_CONFIG = {
  autoFocus: true,
  disableClose: false,
  data: {},
  position: {
    top: '30px',
    right: '25%'
  },
  width: '300px'
};

@Component({
  selector: 'app-search-result-dialog',
  templateUrl: './search-result-dialog.component.html',
  styleUrls: ['./search-result-dialog.component.scss']
})
export class SearchResultDialogComponent implements OnInit, OnDestroy {
  headingDialog = '';
  private searchedObject: Car | Client;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    public dialogRef: MatDialogRef<SearchResultDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { type: string; input: string; data: Observable<Car | Client> }
  ) {}

  ngOnInit() {
    this.parseData();
  }

  parseData() {
    // tslint:disable-next-line
    this.data.data.pipe(takeUntil(this.ngUnsubscribe)).subscribe(result => {
      this.searchedObject = result;
      this.setHeadingDialog();
    });
  }

  setHeadingDialog() {
    if (this.data.type === '') {
      this.headingDialog = 'по вашему запросу ничего не найдено';
    }

    switch (this.data.type) {
      case 'car':
        {
          this.headingDialog =
            this.searchedObject['vin'] !== undefined
              ? `Перейти к машине ${this.searchedObject['brand']} ${this.searchedObject['model']}
            гос.номер: ${this.searchedObject['governmentNumber']}
            vin-номером: ${this.searchedObject['vin']}`
              : `Создать машину с vin-номером: ${this.data.input}`;
        }
        break;
      case 'governmentNumber':
        {
          this.headingDialog =
            this.searchedObject['vin'] !== undefined
              ? `Перейти к машине ${this.searchedObject['brand']} ${this.searchedObject['model']}
            гос.номер: ${this.searchedObject['governmentNumber']} vin: ${this.searchedObject['vin']}`
              : `Создать машину с гос.номером: ${this.data.input}`;
        }
        break;
      case 'client':
        {
          this.headingDialog =
            this.searchedObject['phoneNumber'] !== undefined
              ? `Перейти к ${this.searchedObject['name']} телефон: ${this.searchedObject['phoneNumber']}`
              : `Создать нового клиента с телефонным номером ${this.data.input}`;
        }
        break;
      default:
        this.headingDialog = 'Ничего не найдено c';
    }
  }

  onGoToSearchResult() {
    this.dialogRef.close({
      notNull: this.data.type !== 'none',
      data: this.searchedObject
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
