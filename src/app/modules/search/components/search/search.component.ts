import { Component, EventEmitter, OnDestroy, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';

import { SearchService } from '@app/modules/search/services/search.service';
import {
  SearchResultDialogComponent,
  SEARCH_DIALOG_CONFIG
} from '@app/modules/search/components/search/modals/search-result-dialog/search-result-dialog.component';
// tslint:disable-next-line
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnDestroy {
  @Output() takeResult: EventEmitter<{ type: string; input: string; data: Object }> = new EventEmitter();
  searchField = '';
  searchResult: { type: string; input: string; data: Object };
  private ngUnsubscribe$: Subject<void> = new Subject<void>();

  constructor(private dialog: MatDialog, private searchService: SearchService) {}

  openDialog() {
    // todo: После фикса надо будет править, без засовывания в конфиг data - не работает поиск поэтому так
    SEARCH_DIALOG_CONFIG.data = this.searchResult;
    const dialogRef = this.dialog.open(SearchResultDialogComponent, SEARCH_DIALOG_CONFIG);

    // tslint:disable-next-line
    dialogRef
      .afterClosed()
      .pipe(takeUntil(this.ngUnsubscribe$))
      // tslint:disable-next-line
      .subscribe(result => {
        if (result) {
          this.searchField = '';
          result.notNull
            ? this.takeResult.emit({ ...this.searchResult, data: result.data })
            : this.takeResult.emit({ ...this.searchResult, data: null });
        }
        this.searchResult = null;
      });
  }

  onSearch() {
    this.searchResult = this.searchService.search(this.searchField);
    this.openDialog();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }
}
