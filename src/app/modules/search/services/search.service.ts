import { Injectable } from '@angular/core';
import { FirebaseService } from '@app/services/firebase/firebase.service';
// tslint:disable-next-line
import { Observable } from 'rxjs';
import { Car } from '@app/models/car';
import { Client } from '@app/models/client';

@Injectable()
export class SearchService {
  private searchResult: { type: string; input: string; data: Observable<Car | Client | {}> } = {
    type: '',
    input: '',
    data: null
  };

  constructor(private fbService: FirebaseService) {}

  search(userInput: string) {
    const input = userInput.toUpperCase();
    // search match with vin-number, government number or phone client
    if (input.match(/^\w{13}\d{4}$/)) {
      this.searchResult.type = 'car';
      this.searchResult.input = input;
      this.searchResult.data = this.fbService.getCar(input);
    }
    if (input.match(/^[ABCEHKMOPTXY]\d{3}[ABCEHKMOPTXY]{2}\d{2,3}$/) || input.match(/\b[ABCEHKMOPTXY]\d{6,7}\b/)) {
      this.searchResult.type = 'governmentNumber';
      this.searchResult.input = input;
      this.searchResult.data = this.fbService.findCarGovernmentNumber(input);
    }
    if (input.match(/\b(\+)?\d{10,12}\b/) || input.match(/\b\d{6}\b/)) {
      this.searchResult.type = 'client';
      this.searchResult.input = input.slice(-10);
      this.searchResult.data = this.fbService.getClient(+input.slice(-10));
    }
    return this.searchResult;
  }
}
