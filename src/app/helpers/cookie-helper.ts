import { Injectable } from '@angular/core';

const ONE_YEAR = 365 * 24 * 60 * 60;

@Injectable()
export class CookieHelper {
  private static defaultProps = {
    expires: ONE_YEAR
  };

  static get(name) {
    const matches = document.cookie.match(
      new RegExp('(?:^|; )' + name.replace(/([.$?*|{}()\[\]\\\/+^])/g, '\\$1') + '=([^;]*)')
    );

    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  static set(name, value, options?) {
    options = options || this.defaultProps;

    let expires = options.expires;

    if (expires && typeof expires === 'number') {
      const d = new Date();
      d.setTime(d.getTime() + expires * 1000);
      expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
      options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    let updatedCookie = name + '=' + value;

    for (const propName in options) {
      if (options.hasOwnProperty(propName)) {
        updatedCookie += '; ' + propName;
        const propValue = options[propName];
        if (propValue !== true) {
          updatedCookie += '=' + propValue;
        }
      }
    }

    document.cookie = updatedCookie;
  }

  static delete(name) {
    this.set(name, '', {
      expires: -1
    });
  }
}
