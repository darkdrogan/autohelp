import { Action } from '@ngrx/store';

export enum AuthTypes {
  TRY_LOGIN = '[Auth] Try login',
  SUCCESS_LOGIN = '[Auth] Login successful',
  FAILED_LOGIN = '[Auth] Login failed',
  LOGOUT = '[Auth] Logout'
}

export class TryLogin implements Action {
  readonly type = AuthTypes.TRY_LOGIN;

  constructor(public payload: { email: string; password: string }) {}
}

export class SuccessLogin implements Action {
  readonly type = AuthTypes.SUCCESS_LOGIN;

  constructor(public payload: string) {}
}

export class ErrorLogin implements Action {
  readonly type = AuthTypes.FAILED_LOGIN;
}

export class Logout implements Action {
  readonly type = AuthTypes.LOGOUT;
}
export type AuthActionsUnion = TryLogin | SuccessLogin | ErrorLogin | Logout;
