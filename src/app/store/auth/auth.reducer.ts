import { AuthActionsUnion, AuthTypes } from './auth.actions';

export interface AppState {
  auth: AuthState;
}

export interface AuthState {
  token: string;
}

const initialState: AuthState = {
  token: ''
};

export function authReducer(state = initialState, action: AuthActionsUnion) {
  switch (action.type) {
    case AuthTypes.SUCCESS_LOGIN:
      return {
        ...state,
        token: action.payload
      };
    case AuthTypes.FAILED_LOGIN:
    case AuthTypes.LOGOUT:
      return {
        ...state,
        token: ''
      };
    default:
      return state;
  }
}
