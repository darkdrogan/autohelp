import { Actions, Effect, ofType } from '@ngrx/effects';
import { AuthTypes, TryLogin } from './auth.actions';
import { catchError, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import * as firebase from 'firebase';
import { fromPromise } from 'rxjs/internal-compatibility';
import { Router } from '@angular/router';
// tslint:disable-next-line
import { of } from 'rxjs';
import { ClientTypes } from '@app/store/client/client.actions';
import { CarTypes } from '@app/store/car/car.actions';
import { OrderTypes } from '@app/store/order/order.actions';

@Injectable()
export class AuthEffects {
  @Effect()
  tryLogin = this.actions$.pipe(
    ofType(AuthTypes.TRY_LOGIN),
    map((action: TryLogin) => action.payload),
    switchMap(({ email, password }) => {
      return fromPromise(firebase.auth().signInWithEmailAndPassword(email, password));
    }),
    switchMap(({ user }) => user.getIdToken()),
    tap(() => this.router.navigate(['/catalog'])),
    // tslint:disable-next-line
    mergeMap(token => {
      return [
        { type: AuthTypes.SUCCESS_LOGIN, payload: token },
        { type: ClientTypes.GET_CLIENTS },
        { type: CarTypes.GET_CARS },
        { type: OrderTypes.GET_ORDERS }
      ];
    }),
    catchError(({ code }) => {
      return of({ type: AuthTypes.FAILED_LOGIN });
    })
  );

  constructor(private actions$: Actions, private router: Router) {}
}
