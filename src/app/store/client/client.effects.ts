import { Injectable } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { ClientTypes } from '@app/store/client/client.actions';
import { FirebaseService } from '@app/services/firebase/firebase.service';
import { Client } from '@app/models/client';

@Injectable()
export class ClientEffects {
  @Effect()
  fetchClients = this.actions$.pipe(
    ofType(ClientTypes.GET_CLIENTS),
    switchMap(() => this.fbService.getClients()),
    map((clients: Client[]) => ({ type: ClientTypes.LOAD_CLIENTS, payload: { clients } }))
  );

  constructor(private actions$: Actions, private fbService: FirebaseService) {}
}
