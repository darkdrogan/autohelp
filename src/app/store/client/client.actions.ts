import { Action } from '@ngrx/store';
import { Client } from '@app/models/client';
import { EntityMap, Update } from '@ngrx/entity';

export enum ClientTypes {
  ADD_CLIENT = '[Client] add client',
  GET_CLIENTS = '[Client] Try to load clients',
  LOAD_CLIENTS = '[Client] Load Clients',
  UPSERT_CLIENT = '[Client] Upsert Client',
  // ADD_CLIENTS = '[Client] Add Clients',
  // UPSERT_CLIENTS = '[Client] Upsert Clients',
  UPDATE_CLIENT = '[Client] Update Client',
  // UPDATE_CLIENTS = '[Client] Update Clients',
  MAP_CLIENTS = '[Client] Map Clients',
  DELETE_CLIENT = '[Client] Delete Client'
  // DELETE_CLIENTS = '[Client] Delete Clients',
  // DELETE_CLIENTS_BY_PREDICATE = '[Client] Delete Clients By Predicate',
  // CLEAR_CLIENTS = '[Client] Clear Clients',
}

export class AddClient implements Action {
  readonly type = ClientTypes.ADD_CLIENT;

  constructor(public payload: { client: Client }) {}
}

export class UpsertClient implements Action {
  readonly type = ClientTypes.UPSERT_CLIENT;

  constructor(public payload: { client: Client }) {}
}

export class UpdateClient implements Action {
  readonly type = ClientTypes.UPDATE_CLIENT;

  constructor(public payload: { client: Update<Client> }) {}
}

export class DeleteClient implements Action {
  readonly type = ClientTypes.DELETE_CLIENT;

  constructor(public payload: { id: string }) {}
}

export class MapClients implements Action {
  readonly type = ClientTypes.MAP_CLIENTS;

  constructor(public payload: { entityMap: EntityMap<Client> }) {}
}

export class LoadClients implements Action {
  readonly type = ClientTypes.LOAD_CLIENTS;

  constructor(public payload: { clients: Client[] }) {}
}

export class FetchClients implements Action {
  readonly type = ClientTypes.GET_CLIENTS;
}

export type ClientActionsUnion =
  | AddClient
  | UpsertClient
  | UpdateClient
  | DeleteClient
  | MapClients
  | LoadClients
  | FetchClients;
