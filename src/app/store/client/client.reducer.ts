import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

import { ClientActionsUnion, ClientTypes } from '@app/store/client/client.actions';
import { Client } from '@app/models/client';

export interface State extends EntityState<Client> {
  selectedClientId: number | null;
}

export function selectClientId(a: Client) {
  return a.phoneNumber;
}
export const adapter: EntityAdapter<Client> = createEntityAdapter<Client>({
  selectId: selectClientId,
  sortComparer: false
});

const initialState: State = adapter.getInitialState({
  selectedClientId: null
});

export function clientReducer(state = initialState, action: ClientActionsUnion) {
  switch (action.type) {
    case ClientTypes.ADD_CLIENT:
      return adapter.addOne(action.payload.client, state);
    case ClientTypes.UPSERT_CLIENT:
      return adapter.upsertOne(action.payload.client, state);
    case ClientTypes.UPDATE_CLIENT:
      return adapter.updateOne(action.payload.client, state);
    case ClientTypes.DELETE_CLIENT:
      return adapter.removeOne(action.payload.id, state);
    case ClientTypes.MAP_CLIENTS:
      return adapter.map(action.payload.entityMap, state);
    case ClientTypes.LOAD_CLIENTS:
      return adapter.addAll(action.payload.clients, state);
    default:
      return state;
  }
}

export const getSelectedClientId = (state: State) => state.selectedClientId;

const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();

// select the array of user ids
export const selectUserIds = selectIds;

// select the dictionary of user entities
export const selectUserEntities = selectEntities;

// select the array of users
export const selectAllUsers = selectAll;

// select the total user count
export const selectUserTotal = selectTotal;
