import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

import { CarActionsUnion, CarTypes } from '@app/store/car/car.actions';
import { Car } from '@app/models/car';

export interface State extends EntityState<Car> {
  selectedCarId: number | null;
}

export function selectCarId(a: Car) {
  return a.vin;
}

export const adapter: EntityAdapter<Car> = createEntityAdapter<Car>({
  selectId: selectCarId,
  sortComparer: false
});

export const initialState: State = adapter.getInitialState({
  selectedCarId: null
});

export function carReducer(state = initialState, action: CarActionsUnion) {
  switch (action.type) {
    case CarTypes.ADD_CAR:
      return adapter.addOne(action.payload.car, state);
    case CarTypes.UPSERT_CAR:
      return adapter.upsertOne(action.payload.car, state);
    case CarTypes.UPDATE_CAR:
      return adapter.updateOne(action.payload.car, state);
    case CarTypes.DELETE_CAR:
      return adapter.removeOne(action.payload.id, state);
    case CarTypes.LOAD_CARS:
      return adapter.addAll(action.payload.cars, state);
    default:
      return state;
  }
}

export const getSelectedCarId = (state: State) => state.selectedCarId;

const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();

// select the array of user ids
export const selectCarIds = selectIds;

// select the dictionary of user entities
export const selectCarEntities = selectEntities;

// select the array of users
export const selectAllCars = selectAll;

// select the total user count
export const selectCarTotal = selectTotal;
