import { Action } from '@ngrx/store';
import { Car } from '@app/models/car';
import { Update } from '@ngrx/entity';

export enum CarTypes {
  ADD_CAR = '[Car] Add car',
  UPDATE_CAR = '[Car] Update car',
  UPSERT_CAR = '[Car] Upsert car',
  DELETE_CAR = '[Car] Delete car',
  GET_CARS = '[Car] Get cars from server',
  LOAD_CARS = '[Car] Load cars'
}

export class AddCar implements Action {
  readonly type = CarTypes.ADD_CAR;

  constructor(public payload: { car: Car }) {}
}

export class UpdateCar implements Action {
  readonly type = CarTypes.UPDATE_CAR;

  constructor(public payload: { car: Update<Car> }) {}
}

export class UpsertCar implements Action {
  readonly type = CarTypes.UPSERT_CAR;

  constructor(public payload: { car: Car }) {}
}

export class DeleteCar implements Action {
  readonly type = CarTypes.DELETE_CAR;

  constructor(public payload: { id: string }) {}
}

export class LoadCars implements Action {
  readonly type = CarTypes.LOAD_CARS;

  constructor(public payload: { cars: Car[] }) {}
}

export class GetCars implements Action {
  readonly type = CarTypes.GET_CARS;
}

export type CarActionsUnion = AddCar | UpdateCar | UpsertCar | DeleteCar | LoadCars | GetCars;
