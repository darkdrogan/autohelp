import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { CarTypes } from '@app/store/car/car.actions';
import { map, switchMap } from 'rxjs/operators';
import { FirebaseService } from '@app/services/firebase/firebase.service';
import { Car } from '@app/models/car';

@Injectable()
export class CarEffects {
  @Effect()
  getCar = this.actions$.pipe(
    ofType(CarTypes.GET_CARS),
    switchMap(() => this.fbService.getCars()),
    map((cars: Car[]) => ({ type: CarTypes.LOAD_CARS, payload: { cars } }))
  );

  constructor(private actions$: Actions, private fbService: FirebaseService) {}
}
