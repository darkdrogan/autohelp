import { ActionReducerMap } from '@ngrx/store';

import * as fromAuth from './auth/auth.reducer';
import * as fromClients from './client/client.reducer';
import * as fromCars from './car/car.reducer';
import * as fromOrders from './order/order.reducer';

export interface AppState {
  auth: fromAuth.AuthState;
  orders: fromOrders.State;
  clients: fromClients.State;
  cars: fromCars.State;
}

export const reducers: ActionReducerMap<AppState> = {
  auth: fromAuth.authReducer,
  orders: fromOrders.orderReducer,
  clients: fromClients.clientReducer,
  cars: fromCars.carReducer
};
