import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Order } from '@app/models/order';
import { OrderActionsUnion, OrderTypes } from '@app/store/order/order.actions';

export interface State extends EntityState<Order> {
  selectedOrderId: string | null;
}

export function selectOrderId(a: Order) {
  return a.id;
}

export const adapter: EntityAdapter<Order> = createEntityAdapter<Order>({
  selectId: selectOrderId,
  sortComparer: false
});

const initialState: State = adapter.getInitialState({
  selectedOrderId: null
});

export function orderReducer(state = initialState, action: OrderActionsUnion) {
  switch (action.type) {
    case OrderTypes.ADD_ORDER:
      return adapter.addOne(action.payload.order, state);
    case OrderTypes.DELETE_ORDER:
      return adapter.removeOne(action.payload.id, state);
    case OrderTypes.UPDATE_ORDER:
      return adapter.updateOne(action.payload.order, state);
    case OrderTypes.LOAD_ORDERS:
      return adapter.addAll(action.payload.orders, state);
    case OrderTypes.UPSERT_ORDER:
      return adapter.upsertOne(action.payload.order, state);
    default:
      return state;
  }
}

export const getSelectedOrderId = (state: State) => state.selectedOrderId;

export const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();

// select the array of user ids
export const selectOrderIds = selectIds;

// select the dictionary of user entities
export const selectOrderEntities = selectEntities;

// select the array of users
export const selectAllOrders = selectAll;

// select the total user count
export const selectOrderTotal = selectTotal;
