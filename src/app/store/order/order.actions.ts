import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Order } from '@app/models/order';

export enum OrderTypes {
  ADD_ORDER = '[Order] Add order',
  UPDATE_ORDER = '[Order] Update order',
  DELETE_ORDER = '[Order] Delete order',
  UPSERT_ORDER = '[Order] Upsert order',
  LOAD_ORDERS = '[Order] Load orders',
  GET_ORDERS = '[Order] Get orders'
}

export class AddOrder implements Action {
  readonly type = OrderTypes.ADD_ORDER;

  constructor(public payload: { order: Order }) {}
}

export class UpdateOrder implements Action {
  readonly type = OrderTypes.UPDATE_ORDER;

  constructor(public payload: { order: Update<Order> }) {}
}

export class UpsertOrder implements Action {
  readonly type = OrderTypes.UPSERT_ORDER;

  constructor(public payload: { order: Order }) {}
}

export class DeleteOrder implements Action {
  readonly type = OrderTypes.DELETE_ORDER;

  constructor(public payload: { id: string }) {}
}

export class LoadOrders implements Action {
  readonly type = OrderTypes.LOAD_ORDERS;

  constructor(public payload: { orders: Order[] }) {}
}

export class GetOrders implements Action {
  readonly type = OrderTypes.GET_ORDERS;
}

export type OrderActionsUnion = AddOrder | UpdateOrder | UpsertOrder | DeleteOrder | LoadOrders | GetOrders;
