import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { OrderTypes } from '@app/store/order/order.actions';
import { map, switchMap } from 'rxjs/operators';
import { FirebaseService } from '@app/services/firebase/firebase.service';
import { Order } from '@app/models/order';

@Injectable()
export class OrderEffects {
  @Effect()
  getOrders = this.actions$.pipe(
    ofType(OrderTypes.GET_ORDERS),
    switchMap(() => this.fbService.getOrders()),
    map((orders: Order[]) => ({ type: OrderTypes.LOAD_ORDERS, payload: { orders } }))
  );

  constructor(private actions$: Actions, private fbService: FirebaseService) {}
}
