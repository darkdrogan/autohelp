import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AuthGuard } from '@app/guards/auth.guard';
import { CookieHelper } from '@app/helpers/cookie-helper';
import { SharedModule } from '@app/modules/shared/shared.module';
import { Routing } from './app.routing';
import { AppComponent } from './components/app/app.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HeaderComponent } from './components/header/header.component';
import { SearchModule } from './modules/search/search.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from '@app/store/root-store.reducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from '@app/store/auth/auth.effects';
import { ClientEffects } from '@app/store/client/client.effects';
import { CarEffects } from '@app/store/car/car.effects';
import { OrderEffects } from '@app/store/order/order.effects';

@NgModule({
  declarations: [AppComponent, LoginComponent, NotFoundComponent, HeaderComponent],
  imports: [
    CommonModule,
    BrowserModule,
    Routing,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    SearchModule,
    BrowserAnimationsModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([AuthEffects, ClientEffects, OrderEffects, CarEffects]),
    StoreRouterConnectingModule,
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [AuthGuard, CookieHelper],
  bootstrap: [AppComponent]
})
export class AppModule {}
