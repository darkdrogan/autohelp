import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// tslint:disable-next-line
import { Observable, of } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

import { Car } from '@app/models/car';
import { Client } from '@app/models/client';
import { Order } from '@app/models/order';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  basePath = 'https://autohelp-fed7a.firebaseio.com/';
  headers = new HttpHeaders().append('Content-Type', 'application/json');

  constructor(private http: HttpClient) {}

  addCar(car: Car): Observable<Car> {
    const path = this.basePath + 'cars/' + car.vin + '.json';
    return this.http.put<Car>(path, car, { headers: this.headers }).pipe(catchError(null));
  }

  addClient(client: Client): Observable<Client> {
    const path = this.basePath + 'clients/' + client.phoneNumber + '.json';
    return this.http.put<Client>(path, client, { headers: this.headers }).pipe(catchError(null));
  }

  getClient(phoneNumber: number) {
    const path = this.basePath + 'clients/' + phoneNumber + '.json';
    return this.http.get<Client>(path, { headers: this.headers }).pipe(
      // tslint:disable-next-line
      map(response => {
        if (!response) {
          throw new Error('client not found');
        }
        return response;
      }),
      catchError(err => of({}))
    );
  }

  getClients(): Observable<Client[]> {
    const path = this.basePath + 'clients.json';
    return this.http.get<Client[]>(path).pipe(map(clients => Object.keys(clients).map(item => clients[item])));
  }

  getOrders(): Observable<Order[]> {
    const path = this.basePath + 'orders.json';
    return this.http.get(path).pipe(map(orders => Object.keys(orders).map(item => orders[item])));
  }

  getCars(): Observable<Car[]> {
    const path = this.basePath + 'cars.json';
    return this.http.get<Car[]>(path).pipe(
      take(1),
      map(result => Object.keys(result).map(item => result[item]))
    );
  }

  getCar(vin: string) {
    const path = this.basePath + 'cars/' + vin + '.json';
    return this.http.get<Car>(path).pipe(
      take(1),
      // tslint:disable-next-line
      map(response => {
        if (!response) {
          throw new Error('car not found');
        }
        return response;
      }),
      catchError(err => of({}))
    );
  }

  findCarGovernmentNumber(gNumber: string) {
    return this.getCars().pipe(
      take(1),
      // tslint:disable-next-line
      map(cars => {
        const foundCar = cars.filter(car => car.governmentNumber === gNumber);
        if (!foundCar[0]) throw new Error('car not found');
        return foundCar[0];
      }),
      catchError(err => of({}))
    );
  }
}
