import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatalogComponent } from '@app/pages/catalog/containers/catalog/catalog.component';
import { PageCatalogResolver } from '@app/pages/catalog/containers/catalog/catalog.resolver';

const routes: Routes = [
  {
    path: '',
    component: CatalogComponent,
    resolve: { result: PageCatalogResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule {}
