import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { pluck } from 'rxjs/operators';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {
  catalog$;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.pipe(pluck('result')).subscribe((result) => {
      this.catalog$ = result;
    });
  }
}
