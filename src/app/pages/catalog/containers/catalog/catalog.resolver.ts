import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { AppState } from '@app/store/root-store.reducer';
import { Store } from '@ngrx/store';

@Injectable()
export class PageCatalogResolver implements Resolve<any> {
  result;

  constructor(private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    this.result = [
      {
        id: 1,
        name: 'Audi'
      }
    ];
    return this.result;
  }
}
