import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@app/modules/shared/shared.module';
import { PageCatalogResolver } from './containers/catalog/catalog.resolver';
import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './containers/catalog/catalog.component';

@NgModule({
  imports: [CommonModule, CatalogRoutingModule, SharedModule],
  declarations: [CatalogComponent],
  providers: [PageCatalogResolver]
})
export class CatalogModule {}
