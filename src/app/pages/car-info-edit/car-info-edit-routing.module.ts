import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarInfoEditComponent } from '@app/components/car-info-edit/car-info-edit.component';
import { PageCarInfoEditResolver } from '@app/pages/car-info-edit/containers/car-info-edit/car-info-edit.resolver';

const routes: Routes = [
  {
    path: '',
    component: CarInfoEditComponent,
    resolve: { result: PageCarInfoEditResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarInfoEditRoutingModule {}
