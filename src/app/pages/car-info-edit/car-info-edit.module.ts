import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarInfoEditRoutingModule } from './car-info-edit-routing.module';
import { CarInfoEditComponent } from '@app/components/car-info-edit/car-info-edit.component';
import { PageCarInfoEditResolver } from '@app/pages/car-info-edit/containers/car-info-edit/car-info-edit.resolver';
import { SharedModule } from '@app/modules/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FirebaseService } from '@app/services/firebase/firebase.service';
import { ClientInfoEditComponent } from '@app/components/client-info-edit/client-info-edit.component';

@NgModule({
  imports: [CommonModule, CarInfoEditRoutingModule, SharedModule, FormsModule, ReactiveFormsModule, HttpClientModule],
  declarations: [CarInfoEditComponent, ClientInfoEditComponent],
  providers: [PageCarInfoEditResolver, FirebaseService]
})
export class CarInfoEditModule {}
