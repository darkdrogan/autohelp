import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from '@app/modules/shared/shared.module';
import { IndexComponent } from './containers/index/index.component';
import { PageIndexResolver } from './containers/index/index.resolver';
import { IndexRoutingModule } from './index-routing.module';

@NgModule({
  imports: [CommonModule, IndexRoutingModule, SharedModule],
  declarations: [IndexComponent],
  providers: [PageIndexResolver]
})
export class IndexModule {}
