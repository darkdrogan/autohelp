import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';

import { IndexComponent } from './index.component';
import { SharedModule } from '@app/modules/shared/shared.module';

describe('IndexComponent', () => {
  let component: IndexComponent;
  let fixture: ComponentFixture<IndexComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [IndexComponent],
      imports: [SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(IndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
