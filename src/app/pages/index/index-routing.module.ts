import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IndexComponent } from './containers/index/index.component';
import { PageIndexResolver } from './containers/index/index.resolver';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
    resolve: { result: PageIndexResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IndexRoutingModule {}
